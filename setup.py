from setuptools import setup

setup(name='queuetools',
      version='0.1.1',
      description='QueueTools are some special functions to help with software developing',
      url='http://gitlab.csn.uchile.cl/dpineda/queuetools',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='GPLv3',
      packages=['queuetools'],
      install_requires=['networktools'],
      include_package_data=True,      
      package_dir={'queuetools': 'queuetools'},
      package_data={
          'queuetools': ['../doc', '../docs', '../requeriments.txt', '../tests']},
      zip_safe=False)
